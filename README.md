Examples and software for Albireo
=================================

Various examples and tools for use with the Albireo.

The Albireo is an expansion for Amstrad CPC providing a microSD slot, a serial port, and an USB
host port allowing to connect many devices to the CPC. The card was designed in 2016 but so far
its capacities have not really been used to their full potential.

This repository will collect sample code in the hope that people can reuse it in their applications
and make cool things with the device.

Have fun!
